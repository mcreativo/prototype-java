public class Main
{
    public static void main(String[] args)
    {
        Creator creator = System.getProperty("type", "1").equals("1") ? new Creator1() : new Creator2();
        new Client(creator).doSomething();
    }
}
