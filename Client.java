public class Client
{
    Creator creator;

    public Client(Creator creator)
    {
        this.creator = creator;
    }

    public void doSomething()
    {
        // some code

        Product product = this.creator.createProduct();

        // some code
    }
}
